# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class DeliveryNote(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.delivery_note'

    @classmethod
    def get_reference(cls, product, shipment):
        pattern = {
            'party': shipment.customer.id,
            'address': shipment.delivery_address.id
        }
        return product.get_cross_reference(pattern)

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(DeliveryNote, cls).get_context(
            records, header, data)
        report_context['product_name'] = (lambda product, shipment, lang:
            cls.product_name(product, shipment, lang))
        report_context['product_code'] = (lambda product, shipment:
            cls.product_code(product, shipment))
        return report_context

    @classmethod
    def product_code(cls, product, shipment):
        reference = cls.get_reference(product, shipment)
        if reference:
            return reference.code
        return product.code

    @classmethod
    def product_name(cls, product, shipment, lang):
        with Transaction().set_context(language=lang):
            reference = cls.get_reference(product, shipment)
            if reference:
                return reference.name
            return product.__class__(product.id).name
