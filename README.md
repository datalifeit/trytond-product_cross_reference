datalife_product_cross_reference
================================

The product_cross_reference module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-product_cross_reference/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-product_cross_reference)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
